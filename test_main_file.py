import unittest
import main_file

class TestMainFile(unittest.TestCase):
    def test_main_func(self):
        res = main_file.main_func()
        self.assertTrue(res)


if __name__ == '__main__':
    unittest.main()